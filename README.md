Polycopié de Bridge
===================
de l'association Phi-Sciences
-----------------------------

# Introduction #

Ce polycopié regroupe les cours et les conventions utilisés dans le
cadre du bridge-club de l'association Phi-Sciences.
Il contient une introduction générale sur le bridge, une explication
sur les principales enchères, quelques ressources annexes générales
ainsi qu'un résumé des conventions pouvant être utilisées en
tournois.

# À qui s'adresse-t-il ? #

À tous les joueurs cherchant une formalisation de ces concepts.
Il peut être utilisé sous sa forme compilée (un polycopié PDF) ainsi
qu'à partir de ses sources LaTeX, qui peuvent être repris tout ou
partie pour un document tiers.

# Conditions d'utilisation #

Le polycopié est placé sous licence LaTeX Project Public License
(<https://www.latex-project.org/lppl.txt>).
Il utilise quelques packages LaTeX définissant des macros relatives au
bridge, qui ont pu être modifiés à la marge.
Ces packages contiennent les coordonnées et licences de leurs auteurs
; se référer à eux pour toute question relative à ces packages.

# Remerciements #

Merci aux auteurs des packages utilisés, ainsi qu'aux différents
contributeurs et utilisateurs de ce polycopié qui ont permis de
l'améliorer au fil des versions.


