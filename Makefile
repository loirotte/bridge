TEXENGINE = latexmk -xelatex
BIBENGINE = biber

COURSENAME  = bridge

all: withoutbib

withoutbib:
	$(TEXENGINE) $(COURSENAME)

# withbib:
# 	$(TEXENGINE) $(COURSENAME)
# 	$(BIBENGINE) $(COURSENAME)
# 	$(TEXENGINE) $(COURSENAME)
# 	$(TEXENGINE) $(COURSENAME)
# 	$(TEXENGINE) $(COURSENAME)

clean:
	/bin/rm -f *~ *.dvi *.log *.aux *.flg *.tmp *.ch *.bbl *.blg *.bat *.lof *.toc *.idx *.ind *.ilg *.out *.snm *.nav *.fls *.fdb_latexmk

