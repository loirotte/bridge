%**********************************************************************
%**********************************************************************
\chapter{Principes de base}
%**********************************************************************
%**********************************************************************

%======================================================================
\section{Introduction}
%======================================================================

%----------------------------------------------------------------------
\subsection{Principes généraux}
%----------------------------------------------------------------------

Le bridge est un jeu de carte qui se joue à 4, en opposant 2 paires de
joueurs (comme à la Belote).
Il se joue avec un jeu de 52 cartes, qui sont distribuées
équitablement entre tous les joueurs, soit 13 cartes par joueur.

Contrairement à des jeux comme la Belote ou le tarot, le bridge n'est
pas un jeu de points, mais un jeu de levées.
L'objectif lors d'une partie consiste donc à réaliser un certain
nombre de levées, sur les 13 que comporte la partie, indépendamment des
cartes contenues dans ces levées.

Une partie est constituée de 2 grandes phases de jeu : les
\emph{enchères}, qui permettent de déterminer le type de
\emph{contrat} qui sera joué, et par quel camp il sera joué, et le
\emph{jeu de la carte} en lui-même.
À l'issue de la partie, un \emph{score} est calculé en fonction du
type de contrat joué et du nombre de levées réalisées.

%----------------------------------------------------------------------
\subsection{Les enchères}
%----------------------------------------------------------------------

%......................................................................
\subsubsection{Introduction}
%......................................................................

Au début d'une partie, le \emph{donneur} distribue toutes les cartes
aux joueurs, en les donnant une par une.
Il débute ensuite les enchères.
Pour cela, il a à sa disposition plusieurs types d'enchères :
\begin{itemize}
\item
  les \emph{enchères de contrat}, qui vont de 1\T à 7\SA,
\item
  l'enchère \Passe,
\item
  les enchères de contre (\X) et de surcontre (\XX).
\end{itemize}

%\medskip

Une fois son enchère faite, le joueur qui se trouve à sa gauche
réalise à son tour une enchère, et cela se poursuit dans le sens des
aiguilles d'une montre jusqu'à ce que 3 enchères \Passep consécutives
soient faites.
Arrivé à cette situation, les enchères sont terminées, et le contrat
joué est le dernier annoncé avant les 3 \Passep.

%......................................................................
\subsubsection{Signification des enchères}
%......................................................................

En dehors des \emph{conventions} qui seront introduites par la suite,
les enchères déterminent le type de contrat qui sera joué, à la
couleur ou à sans atout, ainsi que le nombre de levées à réaliser.
Pour calculer le nombre de levées à réaliser par une paire, il suffit
d'ajouter 6 à la hauteur du contrat demandé.
Ainsi, par exemple :
\begin{itemize}
\item
  un contrat de 1\T signifie que 6+1 levées, soit 7 levées, doivent
  être réalisées en considérant l'atout \T,
\item
  un contrat de 3\C signifie que 6+3 levées, soit 9 levées, doivent
  être réalisées en considérant l'atout \C,
\item
  un contrat de 6\SA signifie que 6+6 levées, soit 12 levées, doivent
  être réalisées en ne considérant aucun atout.
  Cela correspond alors à un contrat de \emph{petit chelem} (toutes
  les levées sauf une).
  De même, il existe également des contrats de \emph{grand chelem},
  qui correspondent à la réalisation de toutes les levées.
\end{itemize}

%......................................................................
\subsubsection{Enchaînement des enchères}
%......................................................................

Lors des enchères, chaque joueur qui veut faire une enchère de
contrat est obligé de produire une enchère \emph{supérieure} à la
dernière enchère de contrat réalisée.
Pour cela, les différentes couleurs sont ordonnées dans l'ordre
suivant (de la plus petite à la plus grande) : \T, \K, \C, \P, \SA.
Ainsi, par exemple :
\begin{itemize}
\item
  les enchères de 1\K, 1\C, 1\P, 1\SA, 2\T, etc. sont supérieures à
  l'enchère de 1\T,
\item
  l'enchère à \K immédiatement supérieure à 1\C est 2\K.
\end{itemize}

%\medskip

Chaque joueur a également la possibilité, lorsque son tour
d'enchérir arrive (pas d'enchère à la volée donc, comme cela peut être
le cas à la Belote coinchée), de fournir l'une des enchères suivantes
:
\begin{itemize}
\item
  \Passe,
\item
  \X (contre) : si la dernière enchère de contrat a été produite par
  les adversaires,
\item
  \XX (surcontre) : si les adversaires ont préalablement contré, et
  qu'aucune enchère de contrat n'a été produite depuis.
\end{itemize}

%\medskip

Enfin, notons qu'un joueur qui produit l'enchère \Passep à un
moment donné peut très bien «~reparler~» par la suite, c'est-à-dire
produire d'autres enchères que \Passep.

%----------------------------------------------------------------------
\subsection{Déroulement d'une partie}
%----------------------------------------------------------------------

Comme précisé ci-avant, le donneur commence les enchères.
Une fois celles-ci terminées, le contrat joué est le dernier annoncé
avant les 3 enchères \Passep.
Cela peut être un contrat « simple » (ex : 2\P, 3\SA, 6\K), contré ou
surcontré (ex : 3\SA\X, 7\C\XX).

Dans la ligne (paire) qui remporte les enchères, le premier joueur
à avoir annoncé au cours des enchères la couleur (\T, \K, \C, \P,
\SA) du contrat final est appelé \emph{déclarant}.
C'est lui qui « joue le coup ».
Le joueur qui se trouve à sa gauche sélectionne une carte dans son
jeu et \emph{entame}.
Le partenaire du déclarant (le \emph{répondant}) étale alors son jeu
sur la table (en plaçant les atouts --- s'il y en a --- à sa droite),
et n'a plus le droit d'intervenir au cours de la partie, si ce n'est
pour fournir les cartes que lui demande le déclarant.
Dans cette situation, le répondant est ainsi appelé le \emph{mort}.

Chaque joueur qui remporte un pli joue la première carte du pli
suivant, jusqu'à ce que toutes les cartes soient jouées.
Normalement, chaque joueur marque devant lui les plis que sa ligne a
réalisés, au moyen de la carte qu'il a joué (verticale si le pli est
remporté, horizontale sinon), ce qui permet d'éviter les erreurs de
compte.

Si le déclarant réalise exactement le nombre de levées qu'il a
demandé, on dit qu'il fait «~=~».
Si une levée manque, on dit qu'il fait «~-1~».
S'il a réalisé une levée de mieux que demandé, on dit qu'il fait
«~+1~».
Et ainsi de suite pour les autres différences de levées...

%----------------------------------------------------------------------
\subsection{Le jeu de la carte}
%----------------------------------------------------------------------

Les règles du jeu de la carte sont extrêmement simples (et
occasionnent à l'inverse des maniements parfois très complexes) :
\begin{itemize}
\item
  l'ordre des cartes est celui de la Bataille : As, Roi, Dame, Valet,
  10, ..., 2,
\item
  les joueurs sont uniquement tenus de fournir la couleur demandée,
\item
  un joueur qui ne peut fournir la couleur demandée n'est pas obligée
  de couper,
\item
  il n'existe aucune obligation de « monter » à l'atout, comme cela
  peut être le cas à la Belote par exemple.
\end{itemize}

%----------------------------------------------------------------------
\subsection{Le score}
%----------------------------------------------------------------------

Sans entrer dans les détails du calcul du score, quelques indications
sont données ci-dessous sur la façon dont ce calcul s'opère.
Lorsqu'une partie est remportée (le déclarant réalise au moins son
contrat), seules les levées au-dessus de la 6\ieme levée comptent,
selon le barème suivant :
\begin{itemize}
\item
  les levées à \T et à \K (qui sont appelées les couleurs
  \emph{mineures}), valent 20 points,
\item
  les levées à \C et à \P (qui sont appelées les couleurs
  \emph{majeures}), valent 30 points,
\item
  à \SA, la première levée vaut 40 points, et les suivantes 30
  points.
\end{itemize}

Si le contrat \emph{demandé} correspond à au moins 100 points (donc au
minimum 5\T, 5\K, 4\C, 4\P ou 3\SA), ce contrat représente alors une
\emph{manche}.
Si le contrat demandé correspond à moins de points, il représente une
\emph{partielle}.

La recherche de manches est très importante au bridge, car une prime
très intéressante (300 ou 500 points selon le contexte --- les
vulnérabilités) y est rattachée.
En comparaison, la prime accordée aux partielles réussie n'est que de
50 points...
Et il existe également des primes de petit et grand chelem (500 ou 750
pour le petit, 1000 ou 1500 pour le grand)...

\exes

\begin{tabular}{ll}
  \begin{minipage}{.45\linewidth}
    \begin{itemize}
    \item
      2\C+1 : 140 points,
    \item
      2\T= : 90 points,
    \item
      3\SA= : 400 ou 600 points,
    \end{itemize}
  \end{minipage}
  &
  \begin{minipage}{.45\linewidth}
    \begin{itemize}
    \item
      2\SA+1 : 150 points,
    \item
      4\C= : 420 ou 620 points,
    \item
      4\T+1 : 150 points.
    \end{itemize}
  \end{minipage}
\end{tabular}

Les levées de chute ont des valeurs différentes suivant que le contrat
est simple, contré ou surcontré, et suivant les vulnérabilités !
Au minimum, elles rapportent 50 points aux adversaires par levée
de chute.

Le détail du calcul des scores est donné~\myref{sec:score}.

%======================================================================
\section{Premières enchères}
%======================================================================

%----------------------------------------------------------------------
\subsection{Évaluation des mains}
%----------------------------------------------------------------------

Pour permettre d'évaluer la teneur d'une \emph{main}, un système a été
mis au point par Pierre Albarran.
Ce système, communément utilisé par les joueurs de bridge, est basé
sur les valeurs suivantes :
\begin{itemize}
\item
  As : 4 points,
\item
  Roi : 3 points,
\item
  Dame : 2 points,
\item
  Valet : 1 point.
\end{itemize}

Ces valeurs permettent de déterminer les points d'honneur (points H)
d'une main.
Il est important de bien comprendre qu'il ne servent qu'à son
évaluation, à son potentiel, et qu'ils n'interviennent en aucune façon
dans le calcul du score d'une donne, basé uniquement sur le nombre de
levées.
En utilisant ce système, on totalise donc 40 points au total dans un
jeu complet de 52 cartes.
Et cela fait donc 10 points en moyenne par main...

Si ce système permet assez bien d'évaluer les mains régulières, où
chaque couleur compte au moins 2 cartes, il devient plus limité pour
les mains \emph{distribuées}, c'est-à-dire non régulières.
Pour cela, on l'associe à un autre système, permettant de compter les
points de distribution (points D) :
\begin{itemize}
\item
  Chicane (0 carte dans une couleur) : 3 points,
\item
  Singleton (1 carte dans une couleur) : 2 points,
\item
  Doubleton (2 cartes dans une couleur) : 1 point,
\item
  Chaque carte au dessus de la 5\ieme carte dans une couleur : 1
  point.
\end{itemize}

En additionnant les points obtenus par ces 2 systèmes, on obtient les
points DH.
Attention cependant à ne pas tout cumuler : on ne compte en tout et
pour tout que 3 points pour un Roi sec, et 2 points pour une dame ou
un valet sec.

%----------------------------------------------------------------------
\subsection{Le standard français}
%----------------------------------------------------------------------

Avant d'étudier les premières enchères et leur signification, il est
nécessaire de préciser que \textbf{toutes} les enchères produites à
une table de bridge doivent être comprises par son partenaire --- ce
qui semble la moindre des choses ---, mais \textbf{également} par les
adversaires.
Afin d'unifier les conventions entre joueurs, un standard a été défini
pour les compétitions françaises : c'est le \emph{standard français}
(le «~SEF~»\footnote{Le Système d'Enchères Français.}, en l'occurrence
la «~Majeure 5\ieme~» actuellement).

C'est ce standard qui est étudié ici, et la plupart des conventions
décrites y sont donc conformes.
Cependant, lorsque ce n'est pas le cas, les enchères sont reproduites
de cette manière : \alert{3\T}.
Cela signifie qu'il faut joindre une \emph{alerte} (orale ou
\emph{via} le carton prévu à cet effet) à l'enchère.

Les adversaires, avertis de l'utilisation d'une enchère non conforme
au standard français, peuvent alors, s'ils le souhaitent, demander la
signification de l'enchère au \textbf{partenaire} du joueur ayant
produit l'enchère (et l'alerte).

%----------------------------------------------------------------------
\subsection{Les ouvertures au niveau de 1}
%----------------------------------------------------------------------

L'\emph{ouverture}, c'est-à-dire la première enchère produite autre
que \Passe dans une séquence d'enchères, se fait avec une main
comportant au moins 12 \pts H.
Ainsi, lorsqu'un joueur possède au moins 12 \pts, il va produire une
enchère de contrat, respectant les 3 règles de base suivantes,
\textbf{dans l'ordre} :

\begin{important}
  \begin{enumerate}
  \item
    Avec une majeure au moins 5\ieme, il faut ouvrir de 1\C ou de 1\P.
  \item
    Sans majeure 5\ieme, il faut ouvrir de 1\SA avec,
    \textbf{strictement}, de 15 à 17 \pts et un jeu régulier (mains
    4-3-3-3, 4-4-3-2 ou 5-3-3-2 avec une mineure 5\ieme).
  \item
    Sans majeure 5\ieme, et sans l'ouverture de 1\SA, il faut ouvrir de
    1\T ou 1\K.
  \end{enumerate}
\end{important}

La priorité absolue est donc l'ouverture d'une majeure au moins
5\ieme, lorsque cela est possible bien sûr.
Suivant les distributions, les règles suivantes s'appliquent :
\begin{itemize}
\item
  Avec au moins une majeure au moins 5\ieme :
  \begin{itemize}
  \item
    avec le même nombre de cartes à \C et à \P (5-5 ou 6-6), il faut
    ouvrir de la couleur la plus chère, soit 1\P,
  \item
    avec un bicolore majeur/mineur :
    \begin{itemize}
    \item
      en cas d'égalité, ou si la majeure est plus longue que la
      mineure, il faut ouvrir de la majeure,
    \item
      si la mineure est plus longue que la majeure, il faut tout de
      même ouvrir dans la majeure, à moins de posséder au moins 18
      points DH, auquel cas il faut ouvrir dans la mineure.
    \end{itemize}
  \end{itemize}
\item
  Sans majeure 5\ieme :
  \begin{itemize}
  \item
    avec un nombre de cartes différent à \T et à \K, ouvrir de la
    couleur la plus longue,
  \item
    avec un nombre de cartes égal en mineures :
    \begin{itemize}
    \item
      ouvrir de 1\K avec les 2 mineures au moins 4\iemes,
    \item
      ouvrir de 1\T avec les 2 mineures 3\iemes.
    \end{itemize}
  \item
    Si le SEF «~Majeure 5\ieme~» est standardisé, il intègre de façon
    tout aussi standardisé des variantes sur certains aspects.
    L'une d'elle, le «~carreau 4\ieme~» impose que l'ouverture de 1\K
    se fasse avec au minimum 4 cartes à \K.
    Si cette variante est utilisée, cela implique qu'une ouverture
    d'1\T peut correspondre à une main 4-4-3-2 où le joueur ne possède
    que 2 cartes à \T (!)
  \end{itemize}
\end{itemize}

%----------------------------------------------------------------------
\subsection{Recherche de fit et de manche}
%----------------------------------------------------------------------

Il ne faut pas perdre de vue que les enchères ont pour but de
découvrir le meilleur contrat, au meilleur palier, correspondant à la
force combinée des jeux d'une paire de joueurs.
Pour cela, il faut garder à l'esprit la recherche des objectifs
suivants :

\begin{important}
  \begin{itemize}
  \item
    La recherche d'un \emph{fit} --- au moins 8 cartes dans une
    couleur --- en majeure.
  \item
    La recherche d'une manche, dépendant du nombre de points détenus
    par la paire de joueurs (à partir de 24-25 \pts pour les majeures
    et \SA, à partir de 27-28 \pts pour les mineures).
  \end{itemize}
\end{important}

%======================================================================
\section{L'entame}
%======================================================================

%----------------------------------------------------------------------
\subsection{Principes des différentes entames}
%----------------------------------------------------------------------

Dans un premier temps, les principes des différentes entames
existantes sont présentés.
Bien sûr, toutes ces conventions ne sont pas compatibles.
Il faut donc choisir celles retenues, à la couleur et à \SA.
Et ne pas hésiter à demander aux adversaires quelles conventions ils
ont adoptées, afin de comprendre leurs entames...

\begin{itemize}
\item
  Pair-impair
  \begin{itemize}
  \item
    strict : \fbox{R 8 \ul{4} 3}, \fbox{10 8 \ul{4} 3}
  \item
    petit sous un honneur : \fbox{R 8 \ul{4} 3}, \fbox{10 \ul{8} 4 3}
  \end{itemize}
\item
  Tête de séquence : \fbox{\ul{D} V 10 4}, \fbox{\ul{A} R 8 2},
  \fbox{D \ul{10} 9 3}
\item
  Petit appel sur As et Dame
  \begin{itemize}
  \item
    sur l'As : appeler avec la dame (ou le Roi !) en mettant sa plus
    petite carte
  \item
    sur le Roi
    \begin{itemize}
    \item
      à la couleur : indiquer la parité
    \item
      à \SA : débloquer son éventuel gros honneur (ex : avec l'As
      second, prendre et rejouer) ; indiquer la parité sinon
    \end{itemize}
  \item
    sur la Dame : appeler avec l'As, le Roi ou le Valet ; refuser sinon
  \end{itemize}
\item
  9 et 10 Kantar : l'entame indique une tête de séquence, ou la carte
  au-dessus (10 ou Valet) ainsi qu'un honneur décollé : \fbox{A V
  \ul{10}}, \fbox{R 10 \ul{9}}, \fbox{D 10 \ul{9}}, \fbox{\ul{10} 9 8}
\item
  10 prometteur : même principe, mais avec le 10 uniquement (le 9 ne
  promet rien)
\item
  4\ieme meilleure\footnote{Permettant de savoir combien de cartes
    supérieures à celle d'entame le déclarant possède.
    Il faut utiliser pour cela la \emph{règle des 11} : on retranche
    la valeur de la carte d'entame à 11 pour connaître ce nombre.
  }
  \begin{itemize}
  \item
    strict : \fbox{R 8 4 \ul{3}}, \fbox{10 8 4 \ul{3}}
  \item
    petit sous un honneur : \fbox{R 8 4 \ul{3}}, \fbox{10 \ul{8} 4 3}
  \end{itemize}
\item
  \emph{Top of nothing} : \fbox{\ul{9} 8 7 4 3}, \fbox{\ul{8} 5 4},
  \fbox{\ul{7} 3 2}
\item
  Séquences inversées : \fbox{R \ul{D} 9 2}, \fbox{\ul{D} V 10 3},
  \fbox{D \ul{V} 9 4}, \fbox{\ul{R} D V 2}\\
  Dans une séquence de deux cartes, on entame la plus petite.
  À partir de 3 cartes, on entame normalement tête de séquence.
\end{itemize}

%----------------------------------------------------------------------
\subsection{Recommandations}
%----------------------------------------------------------------------

%......................................................................
\subsubsection{À la couleur}
%......................................................................

\begin{itemize}
\item
  Pair-impair strict
\item
  Tête de séquence
\item
  Appeler par une grosse carte sur l'entame de l'As (du Roi) avec la
  Dame (l'As)
\end{itemize}

\textbf{Cas particulier} : si l'entame est dans le singleton du mort,
indiquer le retour (voir aussi~\myref{sec:sseaa}) :
\begin{itemize}
\item
  Grosse carte : couleur la plus chère
\item
  Petite carte : couleur la moins chère
\item
  Carte moyenne : pas d'indication
\end{itemize}

%......................................................................
\subsubsection{À \SA}
%......................................................................

\begin{itemize}
\item
  4\ieme meilleure, petit sous un honneur (2\ieme meilleure dans une
  longueur au moins 4\ieme sans honneur)
\item
  Tête de séquence
\item
  Petit appel sur As et Dame
\item
  Le Roi promet 3 honneurs (le 10 compte comme un honneur)
\item
  Pair-impair dans une couleur nommée par le partenaire
\end{itemize}

%----------------------------------------------------------------------
\subsection{Exemples récapitulatifs à la couleur}
%----------------------------------------------------------------------

Sur les exemples qui comportent plusieurs possibilités, choisir entre
la demande de parité et la demande d'appel.

\begin{tabular}{p{3cm}p{3cm}p{3cm}p{3cm}p{3cm}}
  \ul{A} \ul{R} D & \ul{A} \ul{R} & \ul{A} R 10 9 & \ul{A} \ul{R} x &
  \ul{A} R x x\\
  \ul{R} D V x & \ul{R} \ul{D} & \ul{R} D 10 9 & \ul{R} D 10 x &
  \ul{R} D x x\\
  \fbox{R \ul{V} 10 x} & \fbox{R V \ul{x} x} & R \ul{10} 9 x & R x
  \ul{x} x & \ul{D} V 10 x\\
  \ul{D} V 9 x & \ul{D} V x x & D x \ul{x} & D x \ul{x} x & \ul{V} 10
  x x\\
  V 9 \ul{x} x & V x \ul{x} & V x \ul{x} x & \ul{x} x & x x \ul{x}\\
  x x \ul{x} x & & & &\\
\end{tabular}

\fbox{
\begin{tabular}{p{3cm}p{3cm}p{3cm}p{3cm}p{1.5cm}}
  \ul{A} D 10 9 & \ul{A} D \ul{x} x & A \ul{V} 10 9 & A \ul{V} 10 x &
  A V \ul{x} x\\
  A \ul{10} 9 x & A x \ul{x} x & & &\\
\end{tabular}
}

\textbf{Attention} : éviter d'entamer dans les séquences encadrées.

%----------------------------------------------------------------------
\subsection{Exemples récapitulatifs à \SA}
%----------------------------------------------------------------------

Sur les exemples qui comportent plusieurs possibilités, choisir entre
la demande de parité et la demande d'appel.

\begin{tabular}{p{3cm}p{3cm}p{3cm}p{3cm}p{3cm}}
  A x x \ul{x} & A 10 x \ul{x} & A \ul{10} 9 x & A x \ul{x} & A V x
  \ul{x}\\
  A \ul{V} 10 x & A V 9 \ul{x} & A D x \ul{x} & A D 10 \ul{x} & A D
  \ul{10} 9\\
  \ul{A} R & \ul{A} R x & \ul{A} \ul{R} \ul{D} & \ul{A} \ul{D} x & A
  \ul{R} \ul{D} x\\
  A R x \ul{x} & \ul{A} R x \ul{x} & \ul{A} R \ul{10} 9 & \ul{R}
  \ul{D} & R \ul{D} x\\
  R \ul{D} x \ul{x}\footnotemark & \ul{R} D 10 x & \ul{R}
  D 10 9 & \ul{R} D V x & \ul{R} D V\\
  R \ul{V} x & R V x \ul{x} & R \ul{V} 10 x & D x \ul{x} & D x x
  \ul{x}\\
  \ul{D} V x \ul{x}\refmark{foot:entdame} & \ul{D} V 10 x & \ul{D} V 9
  x & V x \ul{x} & V x x \ul{x}\\
  V 10 x \ul{x} & V 9 x \ul{x} & \ul{x} x & x \ul{x} x & x \ul{x} x
  x\\
\end{tabular}

\footnotetext{Éviter l'entame de la Dame, sauf indication
contraire.\label{foot:entdame}}

%======================================================================
\section{La signalisation}
%======================================================================

%----------------------------------------------------------------------
\subsection{Principes}
%----------------------------------------------------------------------

Il existe un certain nombre de systèmes permettant de donner une
indication au partenaire, énumérés ci-dessous :
\begin{itemize}
\item 
  Le \textbf{pair-impair}, ou la \emph{parité}, consiste à indiquer
  son nombre de cartes dans une couleur en fonction de l'ordre dans
  lequel on les fournit :
  \begin{itemize}
  \item 
    ascendant : nombre impair de cartes,
  \item 
    descendant : nombre pair de cartes.
  \end{itemize}
\item 
  L'\textbf{appel-refus}, ou \emph{l'attitude} ou l'\emph{appel
  direct}, consiste à montrer son intérêt pour une couleur jouée par
  son partenaire :
  \begin{itemize}
  \item 
    sa plus grosse carte inutile : on est intéressé,
  \item 
    sa plus petite carte : on est désintéressé.
  \end{itemize}
\item 
  L'\textbf{appel de préférence}, consiste à montrer son intérêt pour
  les couleurs restantes :
  \begin{itemize}
  \item 
    une grosse carte : intérêt dans la couleur la plus chère,
  \item 
    une petite carte : intérêt dans la couleur la moins chère.
  \end{itemize}
\end{itemize}

%\medskip

D'une manière générale, il faut essayer de choisir une carte neutre
lorsqu'on a pas d'intérêt particulier. 
Si celle-ci n'est pas lisible, il faut fournir la plus petite.

Dans tous les cas, il faut toujours s'appuyer sur les enchères, la vue
du mort, son propre jeu.
Le renseignement transmis au partenaire doit lui être vraiment utile
(afin de ne pas donner au déclarant une information « gratuite »). 
On ne peut transmettre qu'une seule information à son partenaire ; il
faut donc s'appliquer à faire la bonne sélection en cas de choix. 


%----------------------------------------------------------------------
\subsection{Sur l'entame à \SA}
%----------------------------------------------------------------------

%......................................................................
\subsubsection{Sur l'entame d'une 4ème meilleure}
%......................................................................

Lorsqu'on ne peut pas fournir au dessus du mort, il faut fournir en
pair-impair.

%......................................................................
\subsubsection{Sur l'entame d'un gros honneur (ARD)}
%......................................................................

\begin{itemize}
\item 
  \textbf{Sur entame de l'As (entame de prospection)} : 
  faire un appel direct, avec la Dame ou avec 5\plus cartes.
\item 
  \textbf{Sur l'entame du Roi (ARVXX, RDVX, RD10XX)} :
  débloquer un honneur complémentaire parmi ARD10, sauf s'il y a un
  problème à la vue du mort.
  Dans les autres cas, sans honneur à débloquer, faire du
  pair-impair.\\
  \emph{Exception} : si le mort est singleton, faire un appel direct.
\item 
  \textbf{Sur l'entame de la Dame (DV10(X), DV9(X), ADV(x), RDXX, RDX)} : 
  faire un appel direct pour indiquer ou non un honneur complémentaire
  ARV.
\end{itemize}


%----------------------------------------------------------------------
\subsection{Sur l'entame à l'atout}
\label{sec:sseaa}
%----------------------------------------------------------------------

%......................................................................
\subsubsection{Cas général}
%......................................................................

Tout d'abord, lorsqu'on ne peut pas fournir au dessus du mort, fournir
en pair-impair.

%......................................................................
\subsubsection{Sur l'entame de l'As ou du Roi}
%......................................................................

Faire du pair-impair, sauf :
\begin{itemize}
\item 
  si le mort est singleton, faire un appel de préférence,
\item 
  si le mort possède 3 petites cartes ou le valet 3ème, faire un appel
  direct,
\item 
  avec un doubleton comportant un honneur, il peut être dangereux de
  fournir cet honneur, votre partenaire peut n'avoir qu'ARX ou RDX :
  \begin{itemize}
  \item 
    avec la Dame : ne jamais la fournir, sauf DV secs,
  \item
    avec le Valet : le fournir uniquement sur l'entame de l'As ou si
    on détient V10 secs,
  \item 
    avec le 10 : le fournir toujours, sauf contre indication au mort.
    Exemples :\\
    \begin{center}
      \shortauctionnosouth{V 9 4,\ul{10} 5,,A} mais
      \shortauctionnosouth{V 9 4,10 \ul{5},,R}
    \end{center}
  \item
    avec une séquence d'honneur, lorsque l'entameur ou le mort sont
    maîtres, fournir la tête de séquence sauf contre indication au
    mort.
    Exemple :\\
    \begin{center}
      \begin{tabular}{rl}
        \shortauctionnosouth{A 5 3,V 10 9,,2}
        &
          \begin{minipage}{.5\linewidth}
            \begin{itemize}
            \item si le mort met l'As, fournir le Valet
            \item si le mort met petit, fournir le 9
            \end{itemize}
          \end{minipage}
      \end{tabular}
    \end{center}
  \end{itemize}
\end{itemize}


%----------------------------------------------------------------------
\subsection{En cours de jeu}
%----------------------------------------------------------------------

%......................................................................
\subsubsection{À la couleur}
%......................................................................

Les indications de distribution sont souvent très importantes pour
permettre aux défenseurs d'obtenir un compte précis de la main du
déclarant.
Ceci évite souvent les erreurs dans les défausses.
Il faut donc appliquer le pair-impair. 
L'appel ou le refus peuvent être employés quand une longueur menaçante
au mort oblige les défenseurs à encaisser au plus vite leurs levées
maîtresses.
Il sert également à confirmer ou à infirmer la présence d'un singleton
dans sa main. 


%......................................................................
\subsubsection{À Sans-Atout}
%......................................................................

Là encore, les indications de distribution sont souvent très
importantes pour permettre aux défenseurs d'obtenir un compte précis
de la main du déclarant.
Il faut donc appliquer le pair-impair. 
Il faut utiliser l'appel de préférence lors des défausses pour
indiquer un intérêt pour l'une ou l'autre des couleurs restantes. 

%......................................................................
\subsubsection{Et encore...}
%......................................................................

Avec l'expérience, indépendamment du type de signalisation choisi, il
faut idéalement réussir à anticiper le type d'information attendu par
le partenaire en fonction du déroulement de la partie...
