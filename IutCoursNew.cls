\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{IutCoursNew}[2010/04/30 (Ph. Dosch)]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{joinreport}}
\ProcessOptions

%\LoadClass[a4paper]{joinreport}
\LoadClass{joinreport}

% Précautions d'utilisation
%
% !! Attention !! \textbf{Remplacer les --- par des \ttt}
% !! Vérifier les includegraphics p/r aux center si substitution ruby
% !! Placer des {} avant les symboles de ponctuation doubles dans les
%    \ex{}
% !! Plus de \textsc ! (impropre a priori)
% !! Remplacer les \imptt par des environnements « important »

\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{fancybox}
\usepackage{manfnt}
\usepackage{hyperref}

%%% Les marges
\usepackage{vmargin}
\setpapersize{A4}
%%% Signification des parametres :
%%% \setmargins{leftmargin}{topmargin}{textwidth}{textheight}%
%%%    {headheight}{headsep}{footheight}{footskip}
%%% Ici les 4 derniers parametres sont par defaut a 0
%%% Mais vous pouvez tres bien leur donner une valeur non nulle
%\setmargins{20 mm}{15 mm}{170 mm}{260 mm}{0pt}{0mm}{0pt}{1.2cm}
%\setmargins{20 mm}{20 mm}{170 mm}{250 mm}{0pt}{10mm}{0pt}{1.2cm}
\setmargins{28mm}{5mm}{160mm}{253mm}{1cm}{0.2cm}{0pt}{1.3cm}

\setlength{\parskip}{2 mm}

\author{}

\usepackage{Macros}

% Intérêt ? À remettre dans la classe correspondante pour les TD ?
\def\vmove[#1]#2{\setbox1=\hbox{\raise#1\hbox{#2}}%
\ht1=0pt \dp1=0pt\box1}

% Les boîtes qui vont bien
%\usepackage{boites,boites_exemples}
\usepackage[tikz]{bclogo}
\usepackage{framed}

% Edit du 8/11/12 : un hack pour que XeLaTeX puisse prendre en compte
% les fichiers .mps, qui ne passent plus suite à l'ugrade de la dernière
% ubuntu (12.10) alors que ça passait très bien avant. À retester dans
% une prochaine upgrade, le temps que ce hack soit intégré en standard
% quelque part...
% \makeatletter
% \@namedef{Gin at rule@.mps}#1{{eps}{.mps}{#1}}
% \def\Gin at extensions{.pdf,.eps,.ps,.png,.jpg,.bmp,.pict,.tif,.psd,.mac,.sga,.tga,.gif,.mps}
% \makeatother

%\DeclareGraphicsRule{*}{mps}{*}{} % enables use of MetaPost graphics with pdflatex 

\newenvironment{changemargin}[2]{\begin{list}{}{%
\setlength{\topsep}{0pt}%
\setlength{\leftmargin}{0pt}%
\setlength{\rightmargin}{0pt}%
\setlength{\listparindent}{\parindent}%
\setlength{\itemindent}{\parindent}%
\setlength{\parsep}{0pt plus 1pt}%
\addtolength{\leftmargin}{#1}%
\addtolength{\rightmargin}{#2}%
}\item }{\end{list}}

%\usepackage{algorithmic}
%\usepackage{algorithm}
%\floatname{algorithm}{\textsc{Alg}.}
%%\renewcommand{\algorithmicrequire}{\textbf{\textsc{Entrées:}}}
%\renewcommand{\algorithmicensure}{\underline{\textsc{Résultat:}}}
%\renewcommand{\algorithmicwhile}{\underline{tant que}}
%\renewcommand{\algorithmicdo}{\underline{faire}}
%\renewcommand{\algorithmicendwhile}{\underline{fin tant que}}
%\renewcommand{\algorithmicend}{\underline{fin}}
%\renewcommand{\algorithmicif}{\underline{si}}
%\renewcommand{\algorithmicendif}{\underline{fin si}}
%\renewcommand{\algorithmicelse}{\underline{sinon}}
%\renewcommand{\algorithmicthen}{\underline{alors}}
%\renewcommand{\algorithmicfor}{\underline{pour}}
%\renewcommand{\algorithmicforall}{\underline{pour tout}}
%\renewcommand{\algorithmicdo}{\underline{faire}}
%\renewcommand{\algorithmicendfor}{\underline{fin pour}}
%\renewcommand{\algorithmicrepeat}{\underline{répéter}}
%\renewcommand{\algorithmicuntil}{\underline{jusqu'à ce que}}

%\newenvironment{myalgo}[1]%
%{\noindent\hrulefill~~\emph{#1}~~\hrulefill\\[-1.2mm]%
%\rule{.2mm}{.5cm} \hfill \rule{.2mm}{.5cm}%
%\begin{changemargin}{.5cm}{0cm}%
%\noindent
%\vspace{-1.2cm}%
%\begin{algorithmic}[1]}%
%{\end{algorithmic}%
%\end{changemargin}%
%\vspace{-.5cm}%
%\noindent\rule{.2mm}{.5cm} \hfill \rule{.2mm}{.5cm}\\[-5mm]%
%\rule{\linewidth}{.2mm}}

%\newenvironment{myalgoi}[1]%
%{\noindent\hrulefill~~\emph{#1}~~\hrulefill\\[-.1mm]%
%\rule{.2mm}{.5cm} \hfill \rule{.2mm}{.5cm}%
%\begin{changemargin}{.5cm}{0cm}%
%\noindent
%\vspace{-1.2cm}%
%\begin{algorithmic}[1]}%
%{\end{algorithmic}%
%\end{changemargin}%
%\vspace{-.5cm}%
%\noindent\rule{.2mm}{.5cm} \hfill \rule{.2mm}{.5cm}\\[-5mm]%
%\rule{\linewidth}{.2mm}}

%\newenvironment{important}%
%{\vspace{7mm}\begin{Sbox}\begin{minipage}{.85\linewidth}\vspace{2mm}\bfseries}%
%{\end{minipage}\end{Sbox}\centerline{\boxput*(-1,1)%
%{\hspace{20mm}\includegraphics[width=.6cm]{exclamation.gif.ps}}%
%{\begin{minipage}{.85\linewidth}\psframebox[linewidth=1.pt,framearc=0.5,framesep=10pt]%
%{\TheSbox}\end{minipage}}}}

% \newenvironment{technique}%
% {{\raisebox{-6mm}%
% {\includegraphics[width=1.2cm]{package_utilities}}}%
% \smallskip}%
% {\smallskip\smallskip}

%\newenvironment{important}%
%{\bigskip\begin{boitenumeroteeavecunedoublebarre}%
%{{\vmove[-12mm]{\includegraphics[width=1.5cm]{exclamation}}}}}%
%{\end{boitenumeroteeavecunedoublebarre}}

% \newenvironment{important}%
% {\bigskip\begin{changemargin}{-1.5cm}{0cm}\begin{minipage}{.968\linewidth}\begin{boitenumeroteeavecunedoublebarre}%
% {{\vmove[-12mm]{\includegraphics[width=1.5cm]{exclamation}}}}}%
% {\end{boitenumeroteeavecunedoublebarre}\end{minipage}\end{changemargin}}

\newenvironment{gbar}[1]{%
\def\FrameCommand{{\color{#1}\vrule width 3pt}\colorbox{black!3}}%
\MakeFramed{\advance\hsize-\width\advance\hsize by 8mm \FrameRestore}}%
{\endMakeFramed}

% \newenvironment{important}%
% {\hwfont\footnotesize\vspace*{-2mm}%
% \begin{bclogo}[logo=\bcetoile, barre=none, noborder=true]{}%
% \begin{gbar}{yellow}}
% {\end{gbar}\end{bclogo}}

\newenvironment{important}%
{\hwfont\footnotesize\vspace*{-2mm}%
\begin{flushright}\begin{minipage}{.95\linewidth}\begin{gbar}{yellow}}
{\end{gbar}\end{minipage}\end{flushright}}

\newenvironment{technique}%
{\hwfont\footnotesize\vspace*{-2mm}%
\begin{flushright}\begin{minipage}{.95\linewidth}\begin{gbar}{blue!40}}
{\end{gbar}\end{minipage}\end{flushright}}

\newcommand{\impttp}[1]{{\marginpar{\raisebox{-1em}{\textdbend}}%
\bfseries #1}}

\newcommand{\impttpi}[1]{{\marginpar[{\raisebox{-1em}{\textdbend}}]{}%
\bfseries #1}}

\newenvironment{notezbien}%
{\bfseries}{}

\newenvironment{recommandation}%
{\em}{}

\newcommand{\framesubtitle}[1]{ (\emph{#1})}
\newcommand{\fixme}[1]{\errmessage{#1}}

% \newcommand{\mylisting}[2]{\pagebreak[3]%
% {\fbox{~~#2~~}}\begin{changemargin}{.7cm}{0cm}\vspace*{-0.3cm}%
% \noindent\listinginput{1}{#1}%
% \end{changemargin}\smallskip}

% \newcommand{\mylisting}[2]{\pagebreak[3]\XeTeXdefaultencoding latin1 %
% \begin{changemargin}{.7cm}{0cm}\vspace*{-0.3cm}%
% \noindent\lstinputlisting[title={#2}]{#1}%
% \end{changemargin}\smallskip\XeTeXdefaultencoding auto }

% \newcommand{\mylistingLang}[3]{\pagebreak[3]\XeTeXdefaultencoding latin1 %
% \begin{changemargin}{.7cm}{0cm}\vspace*{-0.3cm}%
% \noindent\lstinputlisting[language={#3},title={#2}]{#1}%
% \end{changemargin}\smallskip\XeTeXdefaultencoding auto }

%% Support de JS

\lstdefinelanguage{JavaScript}
{morekeywords={break,case,catch,%
      const,continue,default,delete,do,else,false,%
      for,function,if,in,instanceof,%
      new,null,%
      return,switch,this,throw,%
      true,try,typeof,var,void,while},%
   sensitive,%
   morecomment=[l]//,%
   morecomment=[s]{/*}{*/},%
   morestring=[b]",%
   morestring=[b]',%
  }%
%%               

%% Support de PHP (version Phil, version de base
%% sans les bons mots-clés)


\lstdefinelanguage{MyPHP}%
  {morekeywords={%
      abstract,and,array(),as,break%
      case,catch,cfunction,class,clone,%
      const,continue,declare,default,do,%
      else,elseif,enddeclare,endfor,endforeach,%
      endif,endswitch,endwhile,extends,final,%
      for,foreach,function,global,goto,%
      if,implements,interface,instanceof,%
      namespace,new,old_function,or,private,%
      protected,public,static,switch,throw,%
      try,use,var,while,xor},%
    sensitive,%
    morecomment=[l]\#,%
    morecomment=[l]//,%
    morecomment=[s]{/*}{*/},%
    morestring=[b]",%
    morestring=[b]'%
  }%

%% Macros relatives aux listings

\newcommand{\mylisting}[2]{\pagebreak[3]%
\begin{changemargin}{.7cm}{0cm}\vspace*{-0.3cm}%
\NoAutoSpacing%
\noindent\lstinputlisting[title={#2}]{#1}%
\end{changemargin}\smallskip}

\newcommand{\mylistingst}[1]{\pagebreak[3]%
\begin{changemargin}{.7cm}{0cm}\vspace*{-0.6cm}%
\NoAutoSpacing%
\noindent\lstinputlisting{#1}%
\end{changemargin}}

\newcommand{\myranglisting}[4]{\pagebreak[3]%
\begin{changemargin}{.7cm}{0cm}\vspace*{-0.3cm}%
\NoAutoSpacing%
\noindent\lstinputlisting[title={#2},firstline={#3},lastline={#4}]{#1}%
\end{changemargin}\smallskip}

\newcommand{\myranglistingst}[3]{\pagebreak[3]%
\begin{changemargin}{.7cm}{0cm}\vspace*{-0.6cm}%
\NoAutoSpacing%
\noindent\lstinputlisting[firstline={#2},lastline={#3}]{#1}%
\end{changemargin}}

\newcommand{\mylistingLang}[3]{\pagebreak[3]%
\begin{changemargin}{.7cm}{0cm}\vspace*{-0.3cm}%
\NoAutoSpacing%
\noindent\lstinputlisting[language={#3},title={#2}]{#1}%
\end{changemargin}\smallskip}

\newcommand{\mylistingLangst}[2]{\pagebreak[3]%
\begin{changemargin}{.7cm}{0cm}\vspace*{-0.6cm}%
\NoAutoSpacing%
\noindent\lstinputlisting[language={#2}]{#1}%
\end{changemargin}}

\newcommand{\mylistingC}[2]{\mylistingLang{#1}{#2}{C}}
\newcommand{\mylistingMake}[2]{\mylistingLang{#1}{#2}{make}}
\newcommand{\mylistingJava}[2]{\mylistingLang{#1}{#2}{Java}}
\newcommand{\mylistingJS}[2]{\mylistingLang{#1}{#2}{JavaScript}}
\newcommand{\mylistingCPP}[2]{\mylistingLang{#1}{#2}{C++}}
\newcommand{\mylistingHTML}[2]{\mylistingLang{#1}{#2}{HTML}}
\newcommand{\mylistingPHP}[2]{\mylistingLang{#1}{#2}{MyPHP}}
\newcommand{\mylistingRuby}[2]{\mylistingLang{#1}{#2}{Ruby}}
\newcommand{\mylistingXML}[2]{\mylistingLang{#1}{#2}{XML}}
\newcommand{\mylistingXSLT}[2]{\mylistingLang{#1}{#2}{XSLT}}

\newcommand{\mylistingCst}[1]{\mylistingLangst{#1}{C}}
\newcommand{\mylistingMakest}[1]{\mylistingLangst{#1}{make}}
\newcommand{\mylistingJavast}[1]{\mylistingLangst{#1}{Java}}
\newcommand{\mylistingJSst}[1]{\mylistingLangst{#1}{JavaScript}}
\newcommand{\mylistingCPPst}[1]{\mylistingLangst{#1}{C++}}
\newcommand{\mylistingHTMLst}[1]{\mylistingLangst{#1}{HTML}}
\newcommand{\mylistingPHPst}[1]{\mylistingLangst{#1}{MyPHP}}
\newcommand{\mylistingRubyst}[1]{\mylistingLangst{#1}{Ruby}}
\newcommand{\mylistingXMLst}[1]{\mylistingLangst{#1}{XML}}
\newcommand{\mylistingXSLTst}[1]{\mylistingLangst{#1}{XSLT}}


\newcommand{\myranglistingLang}[5]{\pagebreak[3]%
\begin{changemargin}{.7cm}{0cm}\vspace*{-0.3cm}%
\NoAutoSpacing%
\noindent\lstinputlisting[language={#3},title={#2},firstline={#4},lastline={#5}]{#1}%
\end{changemargin}\smallskip}

\newcommand{\myranglistingLangst}[4]{\pagebreak[3]%
\begin{changemargin}{.7cm}{0cm}\vspace*{-0.6cm}%
\NoAutoSpacing%
\noindent\lstinputlisting[language={#2},firstline={#3},lastline={#4}]{#1}%
\end{changemargin}}

\newcommand{\myranglistingC}[4]{\myranglistingLang{#1}{#2}{C}{#3}{#4}}
\newcommand{\myranglistingCst}[3]{\myranglistingLangst{#1}{C}{#2}{#3}}

\newcommand{\myranglistingJava}[4]{\myranglistingLang{#1}{#2}{Java}{#3}{#4}}
\newcommand{\myranglistingJavast}[3]{\myranglistingLangst{#1}{Java}{#2}{#3}}

% Pour le C, des macros qui ne font rien, sauf avoir un nom
% différent, ce qui permet de filtrer et de pouvoir générer
% un makefile automatiquement

\newcommand{\myMlistingC}[2]{\mylistingC{#1}{#2}}
\newcommand{\myMlistingCst}[1]{\mylistingCst{#1}}
\newcommand{\myMranglistingC}[4]{\myranglistingC{#1}{#4}{#2}{#3}}
\newcommand{\myMranglistingCst}[3]{\myranglistingCst{#1}{#2}{#3}}

\newcommand{\myMlistingJava}[2]{\mylistingJava{#1}{#2}}
\newcommand{\myMlistingJavast}[1]{\mylistingJavast{#1}}
\newcommand{\myMranglistingJava}[4]{\myranglistingJava{#1}{#4}{#2}{#3}}
\newcommand{\myMranglistingJavast}[3]{\myranglistingJavast{#1}{#2}{#3}}

\newcommand{\ligne}[1]{(ligne #1)}
\newcommand{\lignes}[1]{(lignes #1)}

\newcommand{\IutEntete}[1]%
{\begin{tabular}{lr}
  \begin{minipage}{.22\linewidth}
    \vspace{-4mm}
    \centerline{\includegraphics[width=2.2cm]{verdun.gif.ps}}
  \end{minipage}
  &
  \begin{minipage}{.75\linewidth}
    \rule{\linewidth}{.5mm}\\[-2mm]
    \begin{center}
      {\Large \textbf{\textsc{Iut} Charlemagne}}\\[.7cm]
      {\large \textbf{#1}}
    \end{center}
    \rule{\linewidth}{.6mm}\\
  \end{minipage}
\end{tabular}
\vspace{3mm}}

\newcommand{\IutTitre}[1]%
{\begin{center}
  {\Large
    \textbf{#1}}
\end{center}
\vspace{1cm}}

\newcommand{\IutCoursGarde}[4]%
{
\thispagestyle{empty}

\vspace*{1cm}

\begin{center}
  \includegraphics[width=.9\linewidth]{Logo_IUT-UL_dept_info.png}
\end{center}

\vspace*{2.5cm}

\begin{center}
  \fbox{
    \begin{minipage}{.8\linewidth}
      \vspace*{3cm}
      \begin{center}
        \textbf{\huge #1}\\[1cm]
        \textbf{\Large #2}
      \end{center}
      \vspace*{3.5cm}
    \end{minipage}
  }
\end{center}

\vspace*{2cm}

\textbf{\large #3} \hfill
\textbf{\large Date : #4}

\vfill

\begin{center}
UNIVERSITÉ DE LORRAINE\\
INSTITUT UNIVERSITAIRE DE TECHNOLOGIE\\
2 ter boulevard Charlemagne\\
CS 5227\\
54052 $\bullet$ NANCY cedex\\
-----------------------\\
Tél : 03.83.91.31.31\\
Fax : 03.83.28.13.33\\
\url{http://iut-charlemagne.univ-nancy2.fr/}
\end{center}
}